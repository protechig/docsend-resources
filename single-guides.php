<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package DocSend Resources
 */

get_header(); ?>

	<div class="primary content-area  dsr-contents">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- .primary -->

	<aside class="secondary widget-area dsr-form" role="complementary">
		<div class="form-summary">
			<h3><?php the_field( 'form_heading' ); ?></h3>
			<div class="desc">
			<p>	<?php the_field( 'heading_summary' ); ?></p>
			</div>
		</div>
 <div class="the-form">
	<?php
		$form = get_field( 'single_post_form', 'options' );
		gravity_form( $form, false, true, false, '', true, 1 );
	?>
 </div>
</aside><!-- .secondary -->

<?php get_footer(); ?>
