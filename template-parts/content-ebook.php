<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DocSend Resources
 */

?>

<article <?php post_class(); ?>>
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
	<?php// the_post_thumbnail( 'home-thumb' ); ?>
	<?php
	$image = 'home-thumb';// get the regular sized image
	$image_mobile = 'mobile-thumb';// get the mobile "square" verson of the image
	
	echo '<img src="' . $image . '" srcset="' . $image_mobile . '">';
	?>
	</a>

	<div class="post-contents">
		<header class="entry-header">
			<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
			if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta except-color ">
				<?php dsr_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php
				echo esc_html( ds_custom_exerpt( 20 ) );
			?>
			<br>
			<a class="more-link" href="' . get_permalink() . '">Read more…</a>
		</div><!-- .entry-content -->
	</div>
</article><!-- #post-## -->

