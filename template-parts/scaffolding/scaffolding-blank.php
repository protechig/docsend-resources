<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package DocSend Resources
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'docsend-resources' ); ?></h2>
</section>
