<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DocSend Resources
 */

?>

<article class="webinar-post" <?php post_class(); ?>>
	<header class="entry-header webinar-video">
		<?php the_field( 'webinar_video' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content except-color">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'docsend-resources' ), array(
					'span' => array(
						'class' => array(),
					),
				) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'docsend-resources' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php dsr_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
