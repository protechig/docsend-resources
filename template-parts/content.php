<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DocSend Resources
 */

?>

<article class="post-article"  <?php post_class(); ?>>
<a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
	<?php the_post_thumbnail( 'home-thumb' ); ?>
	</a>
	

	<div class="entry-content">
		<header class="entry-header">
			<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
			if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php dsr_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

		<p>
		<?php
		 echo esc_html( ds_custom_exerpt( 18 ) );
		?>
		<br>
		<a class="more-link" href="<?php the_permalink(); ?>">Read More ...</a>
		</p>
		
	</div><!-- .entry-content -->
</article><!-- #post-## -->
