<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package DocSend Resources
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'docsend-resources' ); ?></a>

	<header class="site-header">
		<div class="header-wrap wrap">

			<div class="site-branding">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="logo">
			<?php
				dsr_display_svg( array(
					'icon'   => 'global-logo',
					'title'  => 'docsend',
					'height' => '24px',
					'width'  => '109px',
					'fill'   => '#fff',
				) );
			?>
			</a>

			</div><!-- .site-branding -->

			<?php dsr_display_header_button(); ?>

			<button type="button" class="off-canvas-open" aria-expanded="false" aria-label="<?php esc_html_e( 'Open Menu', 'docsend-resources' ); ?>">
				<span class="hamburger"></span>
			</button>

			<nav id="site-navigation" class="main-navigation">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'menu_id'        => 'primary-menu',
						'menu_class'     => 'menu dropdown',
					) );
				?>
			</nav><!-- #site-navigation -->
		</div>
	</header><!-- .site-header-->

	 <?php
// The Term Query.
$page_term = get_the_terms( get_the_ID(), 'resource_type' );
?>
 
  <section class="hero-header">
  <div class="wrap border-line"></div>
	 <div class="wrap hero-wraper">
 <?php if ( get_post_type( get_the_ID() ) === 'resources' ) : ?>
<?php foreach ( $page_term as $term ) : ?>
	<!-- if the post type is ebook and its single -->
	
		<?php if ( $term->slug  == 'ebooks' ) : ?>
		<?php if ( is_single() ) : ?> 
		<!--displaying ebbok header on single ebook only -->
		<div class="entry-header">
				<div class="header-summary">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					<p><?php the_field( 'ebook_description' ); ?></p>
					<button class='button primary-btn' id="ebookbtn"  >Get eBook</button>
				</div>
			</div>
			<div class="ebook-cover">
				<img src="<?php the_field( 'ebook_image' ); ?>" alt="ebook cover">
			</div>
			<?php else : ?>	
			<div class="entry-header">
				<div class="header-summary">
				<h1 class="entry-title"><?php the_field( 'docsend_home_header', 'option' ); ?></h1>
					<p><?php the_field( 'header_summery', 'option' ); ?></p>
				</div>
			</div>
			<div class="home-cover">
				<img src="<?php the_field( 'featured_image', 'option' ); ?>" alt="home featured cover">
			</div>	
			<?php endif; ?><!-- ending if single statement on ebbok header -->

			<!-- elseif the post type is webnar -->
		<?php elseif ( $term->slug  == 'webinars' ) : ?>
		<div class="header-summary">
			<h3 class="webinar-tag">WEBINAR</h3>
			<?php the_title( '<h1 class="entry-title webinar-header">', '</h1>' ); ?>
		</div>
		<?php else : ?>
 <!-- else return the default term header -->
		<div class="entry-header">
				<div class="header-summary">
				<h1 class="entry-title"><?php the_field( 'docsend_home_header', 'option' ); ?></h1>
					<p><?php the_field( 'header_summery', 'option' ); ?></p>
				</div>
			</div>

			<div class="home-cover">
				<img src="<?php the_field( 'featured_image', 'option' ); ?>" alt="home featured cover">
			</div>	
		<?php endif; ?>
		<?php endforeach; ?>
		<!-- else return the default header -->
	<?php else : ?>
	<div class="entry-header">
				<div class="header-summary">
				<h1 class="entry-title"><?php the_field( 'docsend_home_header', 'option' ); ?></h1>
					<p><?php the_field( 'header_summery', 'option' ); ?></p>
				</div>
			</div>
			<div class="home-cover">
				<img src="<?php the_field( 'featured_image', 'option' ); ?>" alt="home featured cover">
			</div>	
		<?php endif; ?>

	</div>
 </section>
<div id="content" class="site-content wrap">
