<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package DocSend Resources
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
