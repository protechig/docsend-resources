<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package DocSend Resources
 */

?>

</div><!-- #content -->
<section class="get-demo">
	<div class="get-started wrap">
		<div class="quotation-desc">
			<h2>Ready to get started?</h2>
			<p>DocSend makes it easy to take control of your content and optimize the sales process.</p>
		</div>
		<div class="quotation">
			<a class="button btn" href="<?php echo esc_url( home_url( '/signup' ) ); ?>">Create your account</a>
			<a class="button btn secondary-btn" href="<?php echo esc_url( home_url( '/#' ) ); ?>">Get a Demo</a>
		</div>
	</div>
</section>
<footer class="site-footer">

<div class="footer-widgets wrap">

	<?php dynamic_sidebar( 'footer-1' ); ?>
	<?php dynamic_sidebar( 'footer-2' ); ?>
	<?php dynamic_sidebar( 'footer-3' ); ?>
	<?php dynamic_sidebar( 'footer-4' ); ?>

</div>
<div class="f-border-line wrap"></div>
		<div class="site-info wrap">
			<div class="socials ">
				<div>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="logo">
					<?php
						dsr_display_svg( array(
							'icon'  => 'global-logo',
							'title' => 'docsend',
							'fill'   => 'rgba(255, 255, 255, 0.3)',
							'height' => '20px',
							'width'  => '91px',
						) );
					?>
					</a>
				</div>
				<div>
					<?php dsr_display_social_network_links(); ?>
				</div>
			</div>
			<?php dsr_display_copyright_text(); ?>
		</div><!-- .site-info -->
	</footer><!-- .site-footer container-->
</div><!-- #page -->

<?php wp_footer(); ?>

<nav class="off-canvas-container" aria-hidden="true">
	<button type="button" class="off-canvas-close" aria-label="<?php esc_html_e( 'Close Menu', 'docsend-resources' ); ?>">
		<span class="close"></span>
	</button>

	<?php
		// Mobile menu args.
		$mobile_args = array(
			'theme_location'  => 'mobile',
			'container'       => 'div',
			'container_class' => 'off-canvas-content',
			'container_id'    => '',
			'menu_id'         => 'mobile-menu',
			'menu_class'      => 'mobile-menu',
		);

		// Display the mobile menu.
		wp_nav_menu( $mobile_args );
	?>
</nav>
<div class="off-canvas-screen"></div>
</body>
</html>
