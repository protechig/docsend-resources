<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DocSend Resources
 */

$guides = ds_get_posts_by_type( 'resources', 3, 'resource_type', 'guides' );
$ebooks = ds_get_posts_by_type( 'resources', 3, 'resource_type', 'ebooks' );
get_header();

?>

<div class="primary content-area">
	<main id="main" class="site-main">

	<div class="featured-post wrap">
		<?php
		$recent_posts = ( get_field( 'home_resource_featured', 'options' ) ? get_field( 'home_resource_featured', 'options' ) : get_field( 'home_resource_featured', 'options' ) );
		?>
		<?php if ( $recent_posts ) : ?>
			<?php foreach ( $recent_posts as $post ) : ?>
				<?php setup_postdata( $post ); ?>
		<div class="featured-image">
		<h2 class="featured-header-m"><?php the_field( 'home_featured_post_header', 'options' ); ?></h2>
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
				<?php the_post_thumbnail( 'featured-thumb' ); ?>
			</a>
		</div>
		<div class="featured-desc">
			<h2 class="featured-header"><?php the_field( 'home_featured_post_header', 'options' ); ?></h2>
			<?php the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' ); ?>
			<p class="except-color"><?php echo esc_html( ds_custom_exerpt( 40 ) ); ?></p>
			<a class="more-link" href="<?php the_permalink(); ?>">Read More ....</a>
		</div>
			<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
		<?php endif; ?>	
	</div>


		<div class="resources-post">
					<div class="nav-tab">
						<?php dynamic_sidebar( 'post-nav' ); ?>
					</div>
			<div class="wrap-container wrap">
				<!-- .Guides and reports post section -->
				<div class="post-contents">
					<div class="post-header">
						<h3 class="posts-title">guides & reports</h3>
						<a href="<?php echo esc_url( home_url( 'resource_type/guides' ) ); ?>">See all…</a>
					</div>

					<div id= "guides" class="post-wraper wrap">
						<?php
						foreach ( $guides as $post ) :
						setup_postdata( $post );
						get_template_part( 'template-parts/content', get_post_format() );
						endforeach;
						wp_reset_postdata();
						?>
					</div>
				</div>

				
					<!-- .create acount section -->
				<div class="create-account wrap">
				<div class="desc">
					<h3 class="call-action" >DocSend makes it easy to take control of your content and optimize the sales process.</h3>
					</div>
					<div class="ctr-btn">
						<a class="button btn" href="<?php echo esc_url( home_url( '/signup' ) ); ?>">Create your account</a>
					</div>
				</div>


				<!-- .Ebook post section -->
				<div id="ebooks" class="post-contents">
					<div class="post-header">
						<h3 class="posts-title">ebooks</h3>
						<a href="<?php echo esc_url( home_url( 'resource_type/ebooks' ) ); ?>">See all…</a>
					</div>

					<div id= "ebook" class="post-wraper wrap">
						<?php
						foreach ( $ebooks as $post ) :
						setup_postdata( $post );
						get_template_part( 'template-parts/content', get_post_format() );
						endforeach;
						wp_reset_postdata();
						?>
					</div>
				</div>

				<div id="blogs" class="post-contents">
					<div class="post-header">
						<h3 class="posts-title">Blog posts</h3>
						<a href="<?php echo esc_url( home_url( '/blog' ) ); ?>">See all…</a>
					</div>

					<div id= "blog" class="post-wraper wrap">
						
						<?php if ( get_field( 'featured_blog_posts', 'options' ) ) : ?>
						<?php while ( has_sub_field( 'featured_blog_posts', 'options' ) ) : ?>

						<article class="post-article"  <?php post_class(); ?>>
							<a class="thumbnail" href="<?php the_sub_field( 'post_link', 'options' ); ?>" title="<?php the_title_attribute(); ?>" >
							<img src="<?php the_sub_field( 'blog_featured_image', 'options' ); ?>" alt="">
							</a>
							<div class="entry-content">
								<header class="entry-header">
								<h2 class="entry-title"><a href="<?php the_sub_field( 'post_link', 'options' ); ?>" rel="bookmark"><?php the_sub_field( 'post_title', 'options' ); ?></a></h2>
								</header><!-- .entry-header -->

								<p class="except-color">
								<?php the_sub_field( 'post_description', 'options' ); ?>
								<br>
								<a class="more-link" href="<?php the_sub_field( 'post_link', 'options' ); ?>">Read More</a>
								</p>
							</div><!-- .entry-content -->
						</article><!-- #post-## -->


						<?php endwhile; ?>

						<?php endif; ?>
					</div>
				</div>

			</div>
		</div>
	</main><!-- #main -->
</div><!-- .primary -->
<?php get_footer(); ?>
