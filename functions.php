<?php
/**
 * DocSend Resources functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package DocSend Resources
 */

if ( ! function_exists( 'dsr_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function dsr_setup() {
		/**
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on DocSend Resources, use a find and replace
		 * to change 'docsend-resources' to the name of your theme in all the template files.
		 * You will also need to update the Gulpfile with the new text domain
		 * and matching destination POT file.
		 */
		load_theme_textdomain( 'docsend-resources', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'full-width', 1920, 1080, false );
		add_image_size( 'featured-thumb', 534, 356, true );
		add_image_size( 'home-thumb', 260, 170, true );

		// Register navigation menus.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'docsend-resources' ),
			'mobile'  => esc_html__( 'Mobile Menu', 'docsend-resources' ),
		) );

		/**
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'dsr_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Custom logo support.
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 500,
			'flex-height' => true,
			'flex-width'  => true,
			'header-text' => array( 'site-title', 'site-description' ),
		) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif; // Dsr_setup.
add_action( 'after_setup_theme', 'dsr_setup' );

/**
 * Fires immediately after a comment is inserted into the database.
 *
 * @since 2.8.0
 *
 * @param int $limit    The exerpt limit.
 * @return  $excerpt
 */
function ds_custom_exerpt( $limit ) {
	$excerpt = explode( ' ', get_the_excerpt(), $limit );
	if ( count( $excerpt ) >= $limit ) {
	  array_pop( $excerpt );
	  $excerpt = implode( ' ', $excerpt ) . '...';
	} else {
	  $excerpt = implode( ' ', $excerpt );
	}
	$excerpt = preg_replace( '`[[^]]*]`', '', $excerpt );
	return $excerpt;
  }


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function dsr_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'dsr_content_width', 640 );
}
add_action( 'after_setup_theme', 'dsr_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function dsr_widgets_init() {

	// Define sidebars.
	$sidebars = array(
		'sidebar-1' => esc_html__( 'Sidebar 1', 'docsend-resources' ),
		'post-nav'  => esc_html__( 'post nav Tab', 'DocSend Resources' ),
		'footer-1' => esc_html__( 'Footer 1', 'DocSend Resources' ),
		'footer-2' => esc_html__( 'Footer 2', 'DocSend Resources' ),
		'footer-3' => esc_html__( 'Footer 3', 'DocSend Resources' ),
		'footer-4' => esc_html__( 'Footer 4', 'DocSend Resources' ),
	);

	// Loop through each sidebar and register.
	foreach ( $sidebars as $sidebar_id => $sidebar_name ) {
		register_sidebar( array(
			'name'          => $sidebar_name,
			'id'            => $sidebar_id,
			'description'   => /* translators: the sidebar name */ sprintf( esc_html__( 'Widget area for %s', 'docsend-resources' ), $sidebar_name ),
			'before_widget' => '<aside class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
	}

}
add_action( 'widgets_init', 'dsr_widgets_init' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load styles and scripts.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Load custom ACF features.
 */
require get_template_directory() . '/inc/acf.php';

/**
 * Load custom ACF search functionality.
 */
require get_template_directory() . '/inc/acf-search.php';

/**
 * Load custom filters and hooks.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Load custom queries.
 */
require get_template_directory() . '/inc/queries.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Scaffolding Library.
 */
require get_template_directory() . '/inc/scaffolding.php';
/**
 * Adding form field hide setting.
 */
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
/**
 * Create theme optoins page
 */
if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page(
		array(
			'page_title' => 'Docsend General Settings',
			'menu_title' => 'Docsend Settings',
			'menu_slug'  => 'docsend-general-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		)
		);
}


/**
 * Make a foreach because $terms is an array but it supposed to be only one term
 *
 * @param init $single_template rename template name.
 */
function get_custom_single_template( $single_template ) {
	global $post;

	if ( $post->post_type === 'resources' ) {
	$terms = get_the_terms( $post->ID, 'resource_type' );
		if ( $terms && ! is_wp_error( $terms ) ) {
			foreach ( $terms as $term ) {
				$single_template = include( get_stylesheet_directory() . '/single-' . $term->slug  . '.php' );

			}
		}
	}
	 return $single_template;
}
add_filter( 'single_template', 'get_custom_single_template' );


/**
 * Add taxonomy term  name to body class.
 *
 * @param init $classes the body class.
 */
function add_taxonomy_to_single( $classes ) {
	if ( is_single() ) {
		 global $post;
	$drs_terms = get_the_terms( $post->ID, 'resource_type' );
		if ( $drs_terms && ! is_wp_error( $drs_terms ) ) {
			$classes[] = $drs_terms[0]->slug;
		}
		return $classes;
	}
}
add_filter( 'body_class', 'add_taxonomy_to_single' );
