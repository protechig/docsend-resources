<?php
/**
 * Customizer sections.
 *
 * @package DocSend Resources
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function dsr_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'dsr_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'docsend-resources' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'dsr_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'docsend-resources' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'docsend-resources' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'dsr_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'docsend-resources' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'dsr_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'docsend-resources' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'dsr_customize_sections' );
