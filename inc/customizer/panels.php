<?php
/**
 * Customizer panels.
 *
 * @package DocSend Resources
 */

/**
 * Add a custom panels to attach sections too.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function dsr_customize_panels( $wp_customize ) {

	// Register a new panel.
	$wp_customize->add_panel(
		'site-options', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => esc_html__( 'Site Options', 'docsend-resources' ),
			'description'    => esc_html__( 'Other theme options.', 'docsend-resources' ),
		)
	);
}
add_action( 'customize_register', 'dsr_customize_panels' );
