<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package DocSend Resources
 */

get_header(); ?>

	<div class="primary content-area col-l-8">
		<main id="main" class="site-main">
		<?php
		while ( have_posts() ) :
		the_post();

		get_template_part( 'template-parts/content', 'webinar' );
		?>
		<div class="feature-resource wrap">
			<div class="ds-heading">
				<h2>Other featured content</h2>
			</div>
				<?php
				$related_posts = ( get_field( 'featured', 'options' ) ? get_field( 'featured', 'options' ) : get_field( 'featured', 'options' ) );
				?>
				<?php if ( $related_posts ) : ?>
				<?php foreach ( $related_posts as $post ) : ?>
				<?php setup_postdata( $post ); ?>
				<div class="ebook-featured">
					<?php the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' ); ?>
					<p><?php echo esc_html( ds_custom_exerpt( 20 ) ); ?></p>
					</div>
					<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>	
		</div>
		<div class="ds-view-more wrap">
			<a href="/" class="button ds-btn"> ⟵ View all content in our Resources page</a>
		</div>

		<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
