<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, dsr_scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DocSend Resources
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
			<?php do_action( 'dsr_scaffolding_content' ); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
