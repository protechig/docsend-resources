var button = document.getElementById( 'ebookbtn' );
if ( button ) {
  button.addEventListener( 'click', smoothScroll, false );
  function smoothScroll() {
    document.querySelector( '#gform_submit_button_1' ).scrollIntoView( {
      behavior: 'smooth'
    } );
  }
}
